#include <stdio.h>
int main()
{
    int num;
    
    printf("Enter number: ");
    scanf("%d", &num);
    
    if(num > 0)
    {
        printf("Number is POSITIVE");
    }
    if(num < 0)
    {
        printf("Number is NEGATIVE");
    }
    if(num == 0)
    {
        printf("Number is 0");
    }
    return 0;
}
